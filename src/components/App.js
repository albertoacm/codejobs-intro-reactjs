// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import Header from './Generic/Header';
import Content from './Generic/Main';
import Footer from './Generic/Footer';
//Assets
import './Generic/css/App.css';
// Data
import items from '../data/menu';

class App extends Component {
    static propTypes = {
        children: PropTypes.object.isRequired
    };

    render() {
        const { children } = this.props;

        return (
            <div className="App">
                <Header
                    title="Codejobs"
                    items={items}
                />
                <Content body={children} />
                <Footer copyright="&copy; Sngular 2017" />
            </div>
        );
    }
}

export default App;