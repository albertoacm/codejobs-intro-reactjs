/**
 * Created by administrador on 6/06/17.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Main extends Component {
    static propTypes = {
        children: PropTypes.object.isRequired
    };

    render(){
        const { body } = this.props;
        return (
          <div className="Main">
              {body}
          </div>
        );
    }
    //Version de calculadora
    /*constructor(){
        super();
        console.log('iniciando...');
        this.state = {
            count: 0,
            number1: 0,
            number2: 0,
            result: 0
        };
        this.handleCountClick = this.handleCountClick.bind(this);
        this.handleResultClick = this.handleResultClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentDidMount(){
        console.log('Main Cargado!');
        this.setState({
            count: 1
        });
    }

    handleCountClick(e){
        if(e.target.id === 'add'){
            this.setState({
               count : this.state.count + 1
            });
        }else if(e.target.id === 'minus' && this.state.count > 0){
            this.setState({
               count: this.state.count - 1
            });
        }else{
            this.setState({
               count: 0
            });
        }
    }

    handleInputChange(e){
        if(e.target.id === 'number1'){
            this.setState({
                number1: Number(e.target.value)
            });
        }else{
            this.setState({
                number2: Number(e.target.value)
            });
        }
    }

    handleResultClick(e){
        this.setState({
           result: this.state.number1 + this.state.number2
        });
    }

    render(){
        console.log('render ok!');
        return(
            <div className="App-intro">
                <h2>Counter: { this.state.count }</h2>

                <button id="add" onClick={this.handleCountClick}>+</button>
                <button id="minus" onClick={this.handleCountClick}>-</button>
                <button id="reset" onClick={this.handleCountClick}>Reset</button>

                <h2>Calculator:</h2>
                <p>
                <input id="number1" value={this.state.number1} onChange={this.handleInputChange} type="number"/>
                +
                <input id="number2" value={this.state.number2} onChange={this.handleInputChange} type="number"/>
                <button id="result" onClick={this.handleResultClick}>=</button>
                { this.state.result }
                </p>
            </div>
        );
    }*/
}

export default Main;

