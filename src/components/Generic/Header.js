/**
 * Created by administrador on 6/06/17.
 */
//Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
//Assets
import logo from './images/logo.svg';


class Header extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        items: PropTypes.array.isRequired
    };

    render(){
        console.log(this.props);

        const { title, items } = this.props;
        return(
            <div className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h2>{ title }</h2>
                <ul className="Menu">
                    {items && items.map( (items,key) => <li key={key}><Link to={items.url}>{items.title}</Link></li>)}
                </ul>
            </div>
        );
    }
}

export default Header;

