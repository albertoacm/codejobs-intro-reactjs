/**
 * Created by administrador on 6/06/17.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './css/footer.css';

class Footer extends Component{
    static PropTypes = {
        copyright: PropTypes.string.isRequired
    };

    render(){
        const { copyright = "&copy; 2017" } = this.props;
        return(
            <div className="footer">
                <span dangerouslySetInnerHTML={{ __html : copyright}} />
            </div>
        );
    }
}

export default Footer;