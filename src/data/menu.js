/**
 * Created by administrador on 7/06/17.
 */
export default [
    {
        title:"Home",
        url:"/"
    },
    {
        title:"About Us",
        url:"/about"
    },
    {
        title:"Contact",
        url:"/contact"
    }
];